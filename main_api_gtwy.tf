#
# This sets up an API in API Gateway and publishes endpoints (stages) for
# - production called "api" 
#

resource "aws_api_gateway_rest_api" "this" {
  name               = "${local.deployprefix}${var.api_name}"
  description        = "${var.environment == "production" ? "" : local.deployhead}${var.api_desc}"
  binary_media_types = var.binary_media_types
  tags               = local.tags
}

