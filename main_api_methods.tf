resource "random_id" "lambda_bucket" {
  byte_length = 8
}

resource "aws_s3_bucket" "lambda_bucket" {
  count  = var.use_bucket_for_functions ? 1 : 0
  bucket = lower(replace("${local.deployprefix}${var.api_name}_fera${random_id.lambda_bucket.hex}", "_", "-"))

  acl           = "private"
  force_destroy = true
}

module "api_paths" {
  # source = "./modules/api_resource_tree"
  source = "git::https://gitlab.com/faaster/aws/api-resource-tree.git"


  api_id    = aws_api_gateway_rest_api.this.id
  parent_id = aws_api_gateway_rest_api.this.root_resource_id
  path_tree = var.api_method_map

}

locals {

  methods_map = {
    for meth in module.api_paths.methods_list : "${meth.method}/${meth.fullpath}" => meth
  }
  default_env_vars = {
    ENVIRONMENT  = var.environment
    DEPLOY_LABEL = local.deploylabel
  }
}

module "api_methods" {
  for_each = local.methods_map
  source   = "git::https://gitlab.com/faaster/aws/api-lambda-method.git" # "./modules/api_stages_lambda_method_fn"

  api_id    = aws_api_gateway_rest_api.this.id
  api_name  = aws_api_gateway_rest_api.this.name
  fn_stages = var.stages
  tags      = local.tags

  resource_id                = each.value.api_res_id
  resource_path              = each.value.fullpath
  http_method                = each.value.method
  fn_name_prefix             = each.value.fn_name
  fn_timeout                 = lookup(each.value, "fn_timeout", 1) # default 1s
  fn_mem                     = lookup(each.value, "fn_mem", 256)   # default 256MB
  fn_lang                    = lookup(each.value, "fn_lang", "node20")
  fn_env_vars                = merge(local.default_env_vars, lookup(each.value, "fn_env_vars", {}))
  lambda_role_arn            = module.api_lambda_role.role.arn
  fn_code_bucket_id          = var.use_bucket_for_functions ? aws_s3_bucket.lambda_bucket[0].id : null
  fn_code_use_bucket         = var.use_bucket_for_functions
  use_cors_response_template = local.cors_template
}

module "api_lambda_role" {
  source                = "git::https://gitlab.com/faaster/aws/lambda-role.git"
  name                  = "${local.deployprefix}${var.api_name}"
  path                  = "/tfdeploy/" # Required to be able to maintain; per TFDeployerIAMAccess policy
  enable_lambda_logging = var.enable_lambda_logging
  tags                  = local.tags

}

