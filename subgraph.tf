
locals {
  dot_api         = "${aws_api_gateway_rest_api.this.name}_${md5(aws_api_gateway_rest_api.this.arn)}"
  dot_code_bucket = "${aws_api_gateway_rest_api.this.name}_s3b"
  dot_stages = { for s, ds in aws_api_gateway_stage.this :
    "${s}_${md5(ds.arn)}" => ds
  }
  code_bucket_subgraph = !var.use_bucket_for_functions ? "" : <<-EOT
        ${local.dot_code_bucket} [shape=cylinder, label="S3 \nCode \nBucket"]
  EOT

}

output "graphviz" {
  description = "Description of the module instance for use with Graphviz `dot`."
  value = {
    subgraph = <<-EOT
      subgraph cluster_${local.dot_api} {
        label="API: ${aws_api_gateway_rest_api.this.name}"
        style="rounded,dashed"; labelloc=t; color=grey;

        ${local.code_bucket_subgraph}

        ${module.api_lambda_role.graphviz.subgraph}

        ${join("\n", [for p, m in module.api_methods : m.graphviz.subgraph])}

        ${local.api_domain_subgraph}
      }
    EOT
    nodes = {
      this   = local.dot_api
      stages = { for n, ds in local.dot_stages : ds.stage_name => n }
    }
  }
}
