
locals {
  deployhead = title("${local.deployprefix}deployment ")

  all_stages_map = { for s in var.stages : s.stage_name => s }

  # Take the defaults from the global opts to the module.
  default_stage_options = {
    metrics_enabled        = false
    logging_level          = "ERROR"
    throttling_rate_limit  = 1000
    throttling_burst_limit = 250
  }
}

#
# This is out here because of dependencies without which TF tries
# to make this prematurely
resource "aws_api_gateway_deployment" "this" {
  rest_api_id = aws_api_gateway_rest_api.this.id

  # Need this dependency to ensure API methods are created
  # before we setup the deployment and stages, to avoid error about
  # missing methods
  depends_on = [
    module.api_methods,
    aws_api_gateway_integration_response.cors_opt_200
  ]

  # Need this trigger to ensure the `deployment` gets re-deployed
  # if there are any method changes; otherwise new methods don't
  # show up in any of the existing stages
  triggers = {
    methods_hash = sha1(jsonencode(concat(
      module.api_paths.methods_list,
      values(module.api_methods)[*].trigger_hash,
      values(aws_api_gateway_integration_response.cors_opt_200)[*].response_templates
    )))
  }

  lifecycle {
    create_before_destroy = "true"
  }
}

#
# All the stages
#
resource "aws_api_gateway_stage" "this" {
  for_each      = local.all_stages_map
  rest_api_id   = aws_api_gateway_deployment.this.rest_api_id
  deployment_id = aws_api_gateway_deployment.this.id
  stage_name    = each.key
  variables     = merge(var.stage_vars, { "STAGE_NAME" = each.key }) # add STAGE_NAME var
  description   = "${local.deployhead} stage ${each.key}"
  tags          = local.tags

}

# And the settings for the stages
#
resource "aws_api_gateway_method_settings" "this" {
  for_each    = local.all_stages_map
  rest_api_id = aws_api_gateway_deployment.this.rest_api_id
  stage_name  = aws_api_gateway_stage.this[each.key].stage_name
  method_path = "*/*"

  settings {
    # because we're using optional params, need to check if value is null; lookup() doesn't work.
    metrics_enabled        = (each.value.metrics_enabled != null ? each.value : local.default_stage_options).metrics_enabled
    logging_level          = (each.value.logging_level != null ? each.value : local.default_stage_options).logging_level
    throttling_rate_limit  = (each.value.throttling_rate_limit != null ? each.value : local.default_stage_options).throttling_rate_limit
    throttling_burst_limit = (each.value.throttling_burst_limit != null ? each.value : local.default_stage_options).throttling_burst_limit
  }
}

