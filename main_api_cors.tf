locals {
  cors_enabled  = (var.enable_cors_for != null && length(var.enable_cors_for) > 0)
  cors_template = !local.cors_enabled ? "" : <<EOF
      #set($hOrigin = $input.params().header.Origin)
      #if ($hOrigin)
        #set($hOriginUC = $hOrigin.toUpperCase())
        #set($hAllowed = [ ${join(", ", [for s in var.enable_cors_for : "\"${upper(s)}\""])} ])
        #if ($hAllowed.contains($hOriginUC))
          #set($context.responseOverride.header["Access-Control-Allow-Methods"] = "${join(", ", [for s in var.cors_allow_headers : "${lower(s)}"])}")
          #set($context.responseOverride.header["Access-Control-Allow-Headers"] = "${join(", ", [for s in var.cors_allow_methods : "${lower(s)}"])}")
          #set($context.responseOverride.header["Access-Control-Allow-Origin"] = $hOrigin)
          #set($context.responseOverride.header["Access-Control-Max-Age"] = "${var.cors_max_age}")
          #set($context.responseOverride.header["Access-Control-Allow-Credentials"] = "${var.cors_allow_credentials}")
        #end
      #end
  EOF
}

locals {
  #
  # Check if CORS support is enabled
  cors_path_method_map = local.cors_enabled ? module.api_paths.path_method_map : {}
}

resource "aws_api_gateway_method" "cors_opt" {
  for_each = local.cors_path_method_map

  rest_api_id   = aws_api_gateway_rest_api.this.id
  resource_id   = each.value.api_res_id
  authorization = "NONE"
  http_method   = "OPTIONS"
}

resource "aws_api_gateway_integration" "cors_opt" {
  for_each = local.cors_path_method_map

  rest_api_id = aws_api_gateway_rest_api.this.id
  resource_id = aws_api_gateway_method.cors_opt[each.key].resource_id
  http_method = aws_api_gateway_method.cors_opt[each.key].http_method
  type        = "MOCK"

  request_templates = {
    "application/json" = <<-EOF
    {
      "statusCode" : 200
    }
    EOF
  }
}

resource "aws_api_gateway_method_response" "cors_opt_200" {
  for_each = local.cors_path_method_map

  rest_api_id = aws_api_gateway_rest_api.this.id
  resource_id = aws_api_gateway_integration.cors_opt[each.key].resource_id
  http_method = aws_api_gateway_method.cors_opt[each.key].http_method
  status_code = 200
  response_models = {
    "application/json" = "Empty"
  }
}

resource "aws_api_gateway_integration_response" "cors_opt_200" {
  for_each = local.cors_path_method_map

  rest_api_id = aws_api_gateway_rest_api.this.id
  resource_id = each.value.api_res_id
  http_method = aws_api_gateway_method.cors_opt[each.key].http_method
  status_code = aws_api_gateway_method_response.cors_opt_200[each.key].status_code
  response_templates = {
    "application/json" = <<-EOF
      ${local.cors_template}
      { "status" : "OK"
      }
      EOF
  }
}
