// -------------- domain name if requested
locals {
  // determine if domain is needed
  deploy_domain_list = toset(var.domain_name == null ? [] : [var.domain_name])
}

// We need to make sure we have this cert in `us-east-1` for use with CloudFront.
provider "aws" {
  alias  = "east1cdn"
  region = "us-east-1"
}

#
# If domain name specified, then map it to the deployment stage
#
data "aws_route53_zone" "this" {
  for_each = local.deploy_domain_list
  name     = each.key
}

data "aws_acm_certificate" "this" {
  provider = aws.east1cdn
  for_each = local.deploy_domain_list
  domain   = data.aws_route53_zone.this[each.key].name
  statuses = ["ISSUED"]
}

resource "aws_api_gateway_domain_name" "this" {
  for_each        = local.deploy_domain_list
  domain_name     = data.aws_route53_zone.this[each.key].name
  certificate_arn = data.aws_acm_certificate.this[each.key].arn
  tags            = local.tags
}


resource "aws_route53_record" "this" {
  for_each = local.deploy_domain_list
  zone_id  = data.aws_route53_zone.this[each.key].id
  name     = aws_api_gateway_domain_name.this[each.key].domain_name
  type     = "A"

  alias {
    name                   = aws_api_gateway_domain_name.this[each.key].cloudfront_domain_name
    zone_id                = aws_api_gateway_domain_name.this[each.key].cloudfront_zone_id
    evaluate_target_health = false
  }
}

//--------- path mapping deployment stages if required

resource "aws_api_gateway_base_path_mapping" "this" {
  for_each    = var.domain_name == null ? {} : local.all_stages_map
  api_id      = aws_api_gateway_rest_api.this.id
  stage_name  = aws_api_gateway_stage.this[each.key].stage_name
  base_path   = aws_api_gateway_stage.this[each.key].stage_name
  domain_name = aws_route53_record.this[var.domain_name].fqdn
}

locals {
  api_domain_subgraph = var.domain_name == null ? "" : <<-EOT
    subgraph {
      node [shape=record,style=dashed]
      tls_cert [label = "AWS TLS Cert | ${data.aws_acm_certificate.this[var.domain_name].domain}"]
      node [shape=record,style=solid]
      api_cloudfront [label = "CloudFront | ${aws_api_gateway_domain_name.this[var.domain_name].cloudfront_domain_name}"]
      r53_dns_arec [label = "DNS A Record | ${aws_api_gateway_domain_name.this[var.domain_name].domain_name}"]
      tls_cert -> api_cloudfront -> r53_dns_arec [dir=back]
      
    }
  EOT
}
