output "id" {
  value       = aws_api_gateway_rest_api.this.id
  description = "AWS API gateway's ID"
}

output "root_id" {
  value       = aws_api_gateway_rest_api.this.root_resource_id
  description = "API gateway's root resource's ID"
}

output "raw_urls" {
  description = "The list of naked invocation URLs for for all the stages, including optional additional ones."

  value = concat(
    [aws_api_gateway_deployment.this.invoke_url],
    values(aws_api_gateway_stage.this)[*].invoke_url,
  )
}

# output "x-stages" {
#   value = local.all_stages_map
#   description = "Details about the stages with any merged additional properties per stage, if any. Trouble-shooting only."
# }

# output "x-path_resources" {
#   value = module.api_paths.path_resources
#   description = "Details about the API resources. Trouble-shooting only."
# }

# output "x-methods_list" {
#   value = module.api_paths.methods_list
#   description = "Details about the API methods. Trouble-shooting only."
# }

# output "x_path_method_map" {
#   value = module.api_paths.path_method_map
# }
