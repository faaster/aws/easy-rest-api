variable "api_name" {
  description = "The name of the API under which the function is deployed."
}

variable "api_desc" {
  description = "The description of the API under which the function is deployed."
}

variable "binary_media_types" {
  description = "A list of binary content types that the API will return if you want to limit it in some way."
  type        = list(string)
  default     = []
}

variable "environment" {
  description = "The type of environment (dev, staging, production) for this API. If it's not production, the name is used as a suffix or prefix for AWS resources"
  type        = string
  validation {
    condition     = index(["dev", "staging", "production"], var.environment) >= 0
    error_message = "A valid environment is dev, staging or production."
  }
}

variable "namespace" {
  description = "Optional namespace used when environment is \"dev\". Otherwise ignored."
  type        = string
  default     = null
}

variable "stages" {
  type = list(object({
    stage_name             = string,
    logging_level          = optional(string)
    throttling_rate_limit  = optional(number)
    throttling_burst_limit = optional(number)
    metrics_enabled        = optional(bool)
  }))
  validation {
    condition     = length(var.stages) > 0
    error_message = "At least one stage must be specified."
  }
  description = <<-DOC
    Specify names of all the deployment stages to define as part of this API as a list of
    stage-definition objects where each stage must have at least a stage name.

    E.g.
    ```
    stages = [
      {
        stage_name = "v1"
      },
      {
        stage_name = "v2"
      }
    }
  DOC
}

variable "api_method_map" {
  type        = map(any)
  description = <<-DOC

    Specify the REST API definition as an object nested resources and methods, where resource can be a variable such as `{id}`.

    api_method {
      ONERESOURCE = {
        SUBRESOURCE = {
          "->" = [ # method list
            {
              method = <HTTPMETH>,
              fn_name = <LAMBDA_NAME>,
              fn_timeout = <LAMBDA_TIMEOUT_SECS>,
              fn_mem = <LAMBDA_MEM_MB>,
              fn_lang = <LAMBDA_RUNTIME>,
              fn_env_vars = <OBJ_ENV_VARS>
            }
            ...    # one or more methods per resource
          ]
        }
        ...    # one or more sub-resources
      }
      ...    # one or more resources
    }
  DOC
}

variable "domain_name" {
  description = "Set this attribute to the name of domain to map the API deployment to. The domain must already be available as a Route53 hosted zone, and an AWS Certficate **in `us-east-1` region** must be available for the domain name. Default is no domain."
  default     = null
}

variable "stage_vars" {
  description = "Optional map of stage variables available to all stages in this deployment."
  type        = map(string)
  default     = {}
}

variable "enable_lambda_logging" {
  description = "Set this to `true` to enable Cloud Watch logging access for Lambda functions in this API. Disabled by default."
  default     = false
  type        = bool
}

variable "enable_cors_for" {
  type        = list(string)
  description = "Specify a non-empty list of origin host URLs (e.g. https://example.com) to enable CORS for all APIs."
}

variable "cors_allow_headers" {
  type        = list(string)
  description = "Specify a non-empty list of HTTP headers to allow via CORS for all APIs. Default includes `Content-type`."
  default     = ["Content-type"]
}

variable "cors_allow_methods" {
  type        = list(string)
  description = "Specify a non-empty list of HTTP methods to allow via CORS for all APIs. Default includes `POST` and `OPTIONS`."
  default     = ["POST", "OPTIONS"]
}

variable "cors_allow_credentials" {
  type        = bool
  description = "Enable or disable allowing credentials via CORS. Default is `true`."
  default     = true
}

variable "cors_max_age" {
  type        = number
  description = "Specify the max duration for a CORS session in seconds. Default is 60s."
  default     = 60
}

variable "tags" {
  description = "Map of tags to attach to any resources in this module that support tags. Defaults to `{}`."
  type        = map(string)
  default     = {}
}

variable "use_bucket_for_functions" {
  description = "Set to true to use an S3 bucket to host all the Lambda function code."
  type        = bool
  default     = false
}
