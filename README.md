# FaaSter/AWS/Easy REST API

This is a Terraform module that can be used to _easily_ define and create/deploy a REST API using API Gateway and Lambda.

> Module works; documentation is still a work in progress.

- [Changelog](#changelog)
- [Using the Module](#using-the-module)
- [Module Reference](#module-reference)
  - [Requirements](#requirements)
  - [Providers](#providers)
  - [Modules](#modules)
  - [Resources](#resources)
  - [Inputs](#inputs)
  - [Outputs](#outputs)

## Changelog

See the [repository tags list](https://gitlab.com/faaster/aws/easy-rest-api/-/tags) for the changelog.

## Using the Module

```hcl
module "my_easy_rest_api" {
  source = "git::https://gitlab.com/faaster/aws/easy-rest-api.git"

  api_name              = "my-easy-rest-api"
  api_desc              = "Very simple messaging REST API"
  environment           = var.environment   # typically "dev" or "staging" or "production"
  namespace             = var.namespace     # matters if environment is "dev"
  domain_name           = "my-api.example.com"
  enable_lambda_logging = true  # or false if you don't want any logging to CW

  tags = { # define env vars that will be available to all API methods
  }

  stages = [            # the API deployment stages
    {
      stage_name    = "v1"
      logging_level = "ERROR"   # optional, defaults to ERROR
    },
    {
      stage_name    = "beta"
      logging_level = "INFO"    # optional, defaults to ERROR
    }
  ]

  api_method_map = {    # the API methods
    "message" = {                   # /message
      "{msgId}" = {                 # /message/{msgId}
        "->" = [
          {
            method = "GET",         # GET /message/{msgId}
            fn_name = "get_message",
            fn_lang = "node18",         # optional, defaults to node20
            fn_timeout = 1s,            # optional, defaults to 1s
            fn_mem = 256,               # optional, defaults to 256MB
          },
          {
            method = "DELETE",      # DELETE /message/{msgId}
            fn_name = "del_message"
          }
        ]
      }
      "->" = [
        {
          method = "POST",          # POST /message
          fn_name = "post_message"
        }
      ]
    }
  }

}
```

## Module Reference

### Requirements

| Name                                                                      | Version   |
| ------------------------------------------------------------------------- | --------- |
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.14   |
| <a name="requirement_aws"></a> [aws](#requirement\_aws)                   | >= 3.30.0 |

### Providers

| Name                                                                         | Version   |
| ---------------------------------------------------------------------------- | --------- |
| <a name="provider_aws"></a> [aws](#provider\_aws)                            | >= 3.30.0 |
| <a name="provider_aws.east1cdn"></a> [aws.east1cdn](#provider\_aws.east1cdn) | >= 3.30.0 |
| <a name="provider_random"></a> [random](#provider\_random)                   | n/a       |

### Modules

| Name                                                                                  | Source                                                    | Version |
| ------------------------------------------------------------------------------------- | --------------------------------------------------------- | ------- |
| <a name="module_api_lambda_role"></a> [api\_lambda\_role](#module\_api\_lambda\_role) | git::https://gitlab.com/faaster/aws/lambda-role.git       | n/a     |
| <a name="module_api_methods"></a> [api\_methods](#module\_api\_methods)               | git::https://gitlab.com/faaster/aws/api-lambda-method.git | n/a     |
| <a name="module_api_paths"></a> [api\_paths](#module\_api\_paths)                     | git::https://gitlab.com/faaster/aws/api-resource-tree.git | n/a     |

### Resources

| Name                                                                                                                                                              | Type        |
| ----------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------- |
| [aws_api_gateway_base_path_mapping.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_base_path_mapping)               | resource    |
| [aws_api_gateway_deployment.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_deployment)                             | resource    |
| [aws_api_gateway_domain_name.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_domain_name)                           | resource    |
| [aws_api_gateway_integration.cors_opt](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_integration)                       | resource    |
| [aws_api_gateway_integration_response.cors_opt_200](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_integration_response) | resource    |
| [aws_api_gateway_method.cors_opt](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_method)                                 | resource    |
| [aws_api_gateway_method_response.cors_opt_200](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_method_response)           | resource    |
| [aws_api_gateway_method_settings.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_method_settings)                   | resource    |
| [aws_api_gateway_rest_api.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_rest_api)                                 | resource    |
| [aws_api_gateway_stage.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_stage)                                       | resource    |
| [aws_route53_record.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route53_record)                                             | resource    |
| [aws_s3_bucket.lambda_bucket](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket)                                              | resource    |
| [random_id.lambda_bucket](https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/id)                                                      | resource    |
| [aws_acm_certificate.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/acm_certificate)                                        | data source |
| [aws_caller_identity.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/caller_identity)                                     | data source |
| [aws_region.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/region)                                                       | data source |
| [aws_route53_zone.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/route53_zone)                                              | data source |

### Inputs

| Name                                                                                                             | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                | Type                                                                                                                                                                                                                                                                        | Default                                       | Required |
| ---------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | --------------------------------------------- | :------: |
| <a name="input_api_desc"></a> [api\_desc](#input\_api\_desc)                                                     | The description of the API under which the function is deployed.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           | `any`                                                                                                                                                                                                                                                                       | n/a                                           |   yes    |
| <a name="input_api_method_map"></a> [api\_method\_map](#input\_api\_method\_map)                                 | Specify the REST API definition as an object nested resources and methods, where resource can be a variable such as `{id}`.<br><br>api\_method {<br>  ONERESOURCE = {<br>    SUBRESOURCE = {<br>      "->" = [ # method list<br>        {<br>          method = <HTTPMETH>,<br>          fn\_name = <LAMBDA\_NAME>,<br>          fn\_timeout = <LAMBDA\_TIMEOUT\_SECS>,<br>          fn\_mem = <LAMBDA\_MEM\_MB>,<br>          fn\_lang = <LAMBDA\_RUNTIME>,<br>          fn\_env\_vars = <OBJ\_ENV\_VARS><br>        }<br>        ...    # one or more methods per resource<br>      ]<br>    }<br>    ...    # one or more sub-resources<br>  }<br>  ...    # one or more resources<br>} | `map(any)`                                                                                                                                                                                                                                                                  | n/a                                           |   yes    |
| <a name="input_api_name"></a> [api\_name](#input\_api\_name)                                                     | The name of the API under which the function is deployed.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  | `any`                                                                                                                                                                                                                                                                       | n/a                                           |   yes    |
| <a name="input_binary_media_types"></a> [binary\_media\_types](#input\_binary\_media\_types)                     | A list of binary content types that the API will return if you want to limit it in some way.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               | `list(string)`                                                                                                                                                                                                                                                              | `[]`                                          |    no    |
| <a name="input_cors_allow_credentials"></a> [cors\_allow\_credentials](#input\_cors\_allow\_credentials)         | Enable or disable allowing credentials via CORS. Default is `true`.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        | `bool`                                                                                                                                                                                                                                                                      | `true`                                        |    no    |
| <a name="input_cors_allow_headers"></a> [cors\_allow\_headers](#input\_cors\_allow\_headers)                     | Specify a non-empty list of HTTP headers to allow via CORS for all APIs. Default includes `Content-type`.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  | `list(string)`                                                                                                                                                                                                                                                              | <pre>[<br>  "Content-type"<br>]</pre>         |    no    |
| <a name="input_cors_allow_methods"></a> [cors\_allow\_methods](#input\_cors\_allow\_methods)                     | Specify a non-empty list of HTTP methods to allow via CORS for all APIs. Default includes `POST` and `OPTIONS`.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            | `list(string)`                                                                                                                                                                                                                                                              | <pre>[<br>  "POST",<br>  "OPTIONS"<br>]</pre> |    no    |
| <a name="input_cors_max_age"></a> [cors\_max\_age](#input\_cors\_max\_age)                                       | Specify the max duration for a CORS session in seconds. Default is 60s.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    | `number`                                                                                                                                                                                                                                                                    | `60`                                          |    no    |
| <a name="input_domain_name"></a> [domain\_name](#input\_domain\_name)                                            | Set this attribute to the name of domain to map the API deployment to. The domain must already be available as a Route53 hosted zone, and an AWS Certficate **in `us-east-1` region** must be available for the domain name. Default is no domain.                                                                                                                                                                                                                                                                                                                                                                                                                                         | `any`                                                                                                                                                                                                                                                                       | `null`                                        |    no    |
| <a name="input_enable_cors_for"></a> [enable\_cors\_for](#input\_enable\_cors\_for)                              | Specify a non-empty list of origin host URLs (e.g. https://example.com) to enable CORS for all APIs.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       | `list(string)`                                                                                                                                                                                                                                                              | n/a                                           |   yes    |
| <a name="input_enable_lambda_logging"></a> [enable\_lambda\_logging](#input\_enable\_lambda\_logging)            | Set this to `true` to enable Cloud Watch logging access for Lambda functions in this API. Disabled by default.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             | `bool`                                                                                                                                                                                                                                                                      | `false`                                       |    no    |
| <a name="input_environment"></a> [environment](#input\_environment)                                              | The type of environment (dev, staging, production) for this API. If it's not production, the name is used as a suffix or prefix for AWS resources                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          | `string`                                                                                                                                                                                                                                                                    | n/a                                           |   yes    |
| <a name="input_namespace"></a> [namespace](#input\_namespace)                                                    | Optional namespace used when environment is "dev". Otherwise ignored.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      | `string`                                                                                                                                                                                                                                                                    | `null`                                        |    no    |
| <a name="input_stage_vars"></a> [stage\_vars](#input\_stage\_vars)                                               | Optional map of stage variables available to all stages in this deployment.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                | `map(string)`                                                                                                                                                                                                                                                               | `{}`                                          |    no    |
| <a name="input_stages"></a> [stages](#input\_stages)                                                             | Specify names of all the deployment stages to define as part of this API as a list of<br>stage-definition objects where each stage must have at least a stage name.<br><br>E.g.<pre>stages = [<br>  {<br>    stage_name = "v1"<br>  },<br>  {<br>    stage_name = "v2"<br>  }<br>}</pre>                                                                                                                                                                                                                                                                                                                                                                                                   | <pre>list(object({<br>    stage_name             = string,<br>    logging_level          = optional(string)<br>    throttling_rate_limit  = optional(number)<br>    throttling_burst_limit = optional(number)<br>    metrics_enabled        = optional(bool)<br>  }))</pre> | n/a                                           |   yes    |
| <a name="input_tags"></a> [tags](#input\_tags)                                                                   | Map of tags to attach to any resources in this module that support tags. Defaults to `{}`.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 | `map(string)`                                                                                                                                                                                                                                                               | `{}`                                          |    no    |
| <a name="input_use_bucket_for_functions"></a> [use\_bucket\_for\_functions](#input\_use\_bucket\_for\_functions) | Set to true to use an S3 bucket to host all the Lambda function code.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      | `bool`                                                                                                                                                                                                                                                                      | `false`                                       |    no    |

### Outputs

| Name                                                           | Description                                                                                   |
| -------------------------------------------------------------- | --------------------------------------------------------------------------------------------- |
| <a name="output_graphviz"></a> [graphviz](#output\_graphviz)   | Description of the module instance for use with Graphviz `dot`.                               |
| <a name="output_id"></a> [id](#output\_id)                     | AWS API gateway's ID                                                                          |
| <a name="output_raw_urls"></a> [raw\_urls](#output\_raw\_urls) | The list of naked invocation URLs for for all the stages, including optional additional ones. |
| <a name="output_root_id"></a> [root\_id](#output\_root\_id)    | API gateway's root resource's ID                                                              |
